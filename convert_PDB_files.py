#!/usr/bin/env python3
import argparse
import os
import sys
import glob
import configparser
import subprocess
import json
import cif


def create_parser():
    parser = argparse.ArgumentParser(
             description='USAGE: (python3) convert_PDB_files.py -i path_directory_MD_systems')
    parser.add_argument('-c', '--config', default='config', help='path of config file')
    parser.add_argument('-i', '--input', default='.', help='path of input directory containing MD systems')
    return parser


def read_config(config):
    cfg = configparser.ConfigParser()
    cfg.read(config)
    maxit = cfg["MAXIT"]["maxit"].strip()
    return (maxit)


def make_script_maxit(maxit):
    with open('maxit.sh', "w") as fw:
        line =  '#!/bin/bash\n'
        line += '\n'
        line += 'RCSBROOT=' + maxit + '; export RCSBROOT\n'
        line += 'PATH="$RCSBROOT/bin:"$PATH; export PATH\n'
        line += '\n'
        line += 'maxit -input md.pdb -output md.cif -o 1 -log logfile\n'
        fw.write(line)
    os.chmod('maxit.sh',0o755)


def convert_PDBs(input_dir, maxit):
    mds = glob.glob(input_dir+"/md.*")
    for mddir in mds:
        cwd = os.getcwd()
        os.chdir(mddir)
        if os.path.isfile('md.pdb'):
            print(mddir+'/md.pdb')
            res = subprocess.run(
                    ['obabel','-ipdb','md.pdb','-osdf','-Omd.sdf'],
                    stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
            make_script_maxit(maxit)
            res = subprocess.run(
                    ['./maxit.sh'],
                    stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
            if os.path.isfile('md.cif'):
                res = subprocess.run(
                        ['cifparser.py'],
                        stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
        os.chdir(cwd)


def main():
    parser = create_parser()
    args = parser.parse_args()
    try:
        if args.config:
            config_file = args.config
        if not os.path.isfile(config_file):
            print("Error: not exist file : "+config_file)
            sys.exit()
        if args.input:
            input_dir = args.input
        if not os.path.isdir(input_dir):
            print("Error: not exist input directory : "+input_dir)
            sys.exit()
        (maxit) = read_config(config_file)
        convert_PDBs(input_dir, maxit)
    finally:
        print("END")


if __name__ == "__main__":
    main()
