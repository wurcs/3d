#!/usr/bin/env python3
import argparse
import os
import sys
import re
import configparser
import subprocess


def create_parser():
    parser = argparse.ArgumentParser(
             description='USAGE: (python3) convert_WURCS_to_IUPAC.py -i path_WURCS_list_file')
    parser.add_argument('-c', '--config', default='config', help='path of config file')
    parser.add_argument('-i', '--input', help='path of input WURCS list file')
    parser.add_argument('-o', '--output', help='path of output IUPAC list file')
    return parser


def read_config(config):
    cfg = configparser.ConfigParser()
    cfg.read(config)
    jar = cfg["GlycanFormatConverter"]["jar"].strip()
    export = cfg["GlycanFormatConverter"]["export"].strip()
    return (jar, export)


def convert_WURCS_IUPAC(input_file, output_file, jar, export):
    zen_han_table = str.maketrans('αβ→','ab-')
    with open(output_file, "w") as fw:
        with open(input_file, "r") as fr:
            lines = fr.readlines()
            for line in lines:
                if len(line.strip().split('WURCS=')) == 2:
                    if line.strip().startswith('#'):
                        continue
                    wurcs = line.strip().split('WURCS=')[1]
                    print(wurcs)
                    # python3.7 or later
                    #res = subprocess.run(
                    #        ["java","-jar",GlycanFormatConverter,"-i","WURCS",
                    #                "-e",GlycanFormatConverter_export,"-seq",wurcs],
                    #        capture_output=True, text=True)
                    res = subprocess.run(
                            ["java","-jar",jar,"-i","WURCS","-e",export,"-seq",'WURCS='+wurcs],
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
                    stdoutlines = res.stdout.splitlines()
                    nline = len(stdoutlines)
                    endline = stdoutlines[nline-1].strip()
                    if export=='IUPAC-Extended':
                        endline = endline.translate(zen_han_table)
                    # 
                    # In GLYCAM gmml, the Acelyl group is 'A', but in GlycanFormatConverter, it is 'Ar',
                    # so it is substituted.
                    #
                    if export=='GlycanWeb':
                        endline = re.sub('([1-9])Ac,', '\\1A,', endline)
                        endline = re.sub('([1-9])Ac]', '\\1A]', endline)
                    fw.write(endline+'\n')


def main():
    parser = create_parser()
    args = parser.parse_args()
    try:
        if args.config:
            config_file = args.config
        if not os.path.isfile(config_file):
            print("Error: not exist file : "+config_file)
            sys.exit()
        if args.input:
            input_file = args.input
        if not os.path.isfile(input_file):
            print("Error: not exist file : "+input_file)
            sys.exit()
        (jar, export) = read_config(config_file)
        if args.output:
            output_file = args.output
        else:
            filename = os.path.basename(input_file).rsplit('.',1)[0]
            output_file = filename+'.'+export+'.iupac'
        print('output file : '+output_file)
        print('GlycanFormatConverter export : '+export)
        convert_WURCS_IUPAC(input_file, output_file, jar, export)
    finally:
        print("END")


if __name__ == "__main__":
    main()
