#!/usr/bin/env python3
import argparse
import os
import sys
import glob
import configparser
import subprocess
import json
import cif


def create_parser():
    parser = argparse.ArgumentParser(
             description='USAGE: (python3) convert_PDB_files_for_3D_SNFG.py -i path_directory_MD_systems')
    parser.add_argument('-c', '--config', default='config', help='path of config file')
    parser.add_argument('-i', '--input', default='.', help='path of input directory containing MD systems')
    return parser


def read_config(config):
    cfg = configparser.ConfigParser()
    cfg.read(config)
    maxit = cfg["MAXIT"]["maxit"].strip()
    return (maxit)


def make_script_maxit(maxit):
    with open('maxit_snfg.sh', "w") as fw:
        line =  '#!/bin/bash\n'
        line += '\n'
        line += 'RCSBROOT=' + maxit + '; export RCSBROOT\n'
        line += 'PATH="$RCSBROOT/bin:"$PATH; export PATH\n'
        line += '\n'
        line += 'maxit -input md_snfg.pdb -output md_snfg.cif -o 1 -log logfile_snfg\n'
        fw.write(line)
    os.chmod('maxit_snfg.sh',0o755)


def get_res_list(target, line):
    idx = line.find(target)
    r = line[idx+len(target):]
    r = r.strip().strip(']')
    return r.split()


def convert_PDBs(input_dir):
    wurcs_3d = os.environ.get('WURCS_3D_HOME')
    with open(wurcs_3d+'/vmd/vmd.rc', "r") as f:
        vmdrc = f.readlines()
    for line in vmdrc:
        line = line.split('#')[0]

        ### Filled sphere ###
        target = 'set Glc_common [list'
        if(line.find(target) > 0):
            Glc_common = get_res_list(target, line)
        target = 'set Glc_glycam [list'
        if(line.find(target) > 0):
            Glc_glycam = get_res_list(target, line)

        target = 'set Man_common [list'
        if(line.find(target) > 0):
            Man_common = get_res_list(target, line)
        target = 'set Man_glycam [list'
        if(line.find(target) > 0):
            Man_glycam = get_res_list(target, line)

        target = 'set Gal_common [list'
        if(line.find(target) > 0):
            Gal_common = get_res_list(target, line)
        target = 'set Gal_glycam [list'
        if(line.find(target) > 0):
            Gal_glycam = get_res_list(target, line)

        target = 'set Gul_common [list'
        if(line.find(target) > 0):
            Gul_common = get_res_list(target, line)
        target = 'set Gul_glycam [list'
        if(line.find(target) > 0):
            Gul_glycam = get_res_list(target, line)

        target = 'set Alt_common [list'
        if(line.find(target) > 0):
            Alt_common = get_res_list(target, line)
        target = 'set Alt_glycam [list'
        if(line.find(target) > 0):
            Alt_glycam = get_res_list(target, line)

        target = 'set All_common [list'
        if(line.find(target) > 0):
            All_common = get_res_list(target, line)
        target = 'set All_glycam [list'
        if(line.find(target) > 0):
            All_glycam = get_res_list(target, line)

        target = 'set Tal_common [list'
        if(line.find(target) > 0):
            Tal_common = get_res_list(target, line)
        target = 'set Tal_glycam [list'
        if(line.find(target) > 0):
            Tal_glycam = get_res_list(target, line)

        target = 'set Ido_common [list'
        if(line.find(target) > 0):
            Ido_common = get_res_list(target, line)
        target = 'set Ido_glycam [list'
        if(line.find(target) > 0):
            Ido_glycam = get_res_list(target, line)

        ### Filled cube ###
        target = 'set GlcNAc_common [list'
        if(line.find(target) > 0):
            GlcNAc_common = get_res_list(target, line)
        target = 'set GlcNAc_glycam [list'
        if(line.find(target) > 0):
            GlcNAc_glycam = get_res_list(target, line)

        target = 'set ManNAc_common [list'
        if(line.find(target) > 0):
            ManNAc_common = get_res_list(target, line)
            if len(ManNAc_common) == 0:
                ManNAc_common.append('BM3')
        target = 'set ManNAc_glycam [list'
        if(line.find(target) > 0):
            ManNAc_glycam = get_res_list(target, line)

        target = 'set GalNAc_common [list'
        if(line.find(target) > 0):
            GalNAc_common = get_res_list(target, line)
        target = 'set GalNAc_glycam'
        if(line.find(target) > 0):
            GalNAc_glycam = get_res_list(target, line)

        target = 'set GulNAc_common [list'
        if(line.find(target) > 0):
            GulNAc_common = get_res_list(target, line)
        target = 'set GulNAc_glycam [list'
        if(line.find(target) > 0):
            GulNAc_glycam = get_res_list(target, line)

        target = 'set AltNAc_common [list'
        if(line.find(target) > 0):
            AltNAc_common = get_res_list(target, line)
        target = 'set AltNAc_glycam [list'
        if(line.find(target) > 0):
            AltNAc_glycam = get_res_list(target, line)

        target = 'set AllNAc_common [list'
        if(line.find(target) > 0):
            AllNAc_common = get_res_list(target, line)
        target = 'set AllNAc_glycam [list'
        if(line.find(target) > 0):
            AllNAc_glycam = get_res_list(target, line)

        target = 'set TalNAc_common [list'
        if(line.find(target) > 0):
            TalNAc_common = get_res_list(target, line)
        target = 'set TalNAc_glycam [list'
        if(line.find(target) > 0):
            TalNAc_glycam = get_res_list(target, line)

        target = 'set IdoNAc_common [list'
        if(line.find(target) > 0):
            IdoNAc_common = get_res_list(target, line)
        target = 'set IdoNAc_glycam [list'
        if(line.find(target) > 0):
            IdoNAc_glycam = get_res_list(target, line)

        ### Crossed cube ###
        target = 'set GlcN_common [list'
        if(line.find(target) > 0):
            GlcN_common = get_res_list(target, line)
        target = 'set GlcN_glycam [list'
        if(line.find(target) > 0):
            GlcN_glycam = get_res_list(target, line)

        target = 'set ManN_common [list'
        if(line.find(target) > 0):
            ManN_common = get_res_list(target, line)
        target = 'set ManN_glycam [list'
        if(line.find(target) > 0):
            ManN_glycam = get_res_list(target, line)

        target = 'set GalN_common [list'
        if(line.find(target) > 0):
            GalN_common = get_res_list(target, line)
        target = 'set GalN_glycam [list'
        if(line.find(target) > 0):
            GalN_glycam = get_res_list(target, line)

        target = 'set GulN_common [list'
        if(line.find(target) > 0):
            GulN_common = get_res_list(target, line)
        target = 'set GulN_glycam [list'
        if(line.find(target) > 0):
            GulN_glycam = get_res_list(target, line)

        target = 'set AltN_common [list'
        if(line.find(target) > 0):
            AltN_common = get_res_list(target, line)
        target = 'set AltN_glycam [list'
        if(line.find(target) > 0):
            AltN_glycam = get_res_list(target, line)

        target = 'set AllN_common [list'
        if(line.find(target) > 0):
            AllN_common = get_res_list(target, line)
        target = 'set AllN_glycam [list'
        if(line.find(target) > 0):
            AllN_glycam = get_res_list(target, line)

        target = 'set TalN_common [list'
        if(line.find(target) > 0):
            TalN_common = get_res_list(target, line)
        target = 'set TalN_glycam [list'
        if(line.find(target) > 0):
            TalN_glycam = get_res_list(target, line)

        target = 'set IdoN_common [list'
        if(line.find(target) > 0):
            IdoN_common = get_res_list(target, line)
        target = 'set IdoN_glycam [list'
        if(line.find(target) > 0):
            IdoN_glycam = get_res_list(target, line)

        ### Divided diamond ###
        target = 'set GlcA_common [list'
        if(line.find(target) > 0):
            GlcA_common = get_res_list(target, line)
        target = 'set GlcA_glycam [list'
        if(line.find(target) > 0):
            GlcA_glycam = get_res_list(target, line)

        target = 'set ManA_common [list'
        if(line.find(target) > 0):
            ManA_common = get_res_list(target, line)
        target = 'set ManA_glycam [list'
        if(line.find(target) > 0):
            ManA_glycam = get_res_list(target, line)

        target = 'set GalA_common [list'
        if(line.find(target) > 0):
            GalA_common = get_res_list(target, line)
        target = 'set GalA_glycam [list'
        if(line.find(target) > 0):
            GalA_glycam = get_res_list(target, line)

        target = 'set GulA_common [list'
        if(line.find(target) > 0):
            GulA_common = get_res_list(target, line)
        target = 'set GulA_glycam [list'
        if(line.find(target) > 0):
            GulA_glycam = get_res_list(target, line)

        target = 'set AltA_common [list'
        if(line.find(target) > 0):
            AltA_common = get_res_list(target, line)
        target = 'set AltA_glycam [list'
        if(line.find(target) > 0):
            AltA_glycam = get_res_list(target, line)

        target = 'set AllA_common [list'
        if(line.find(target) > 0):
            AllA_common = get_res_list(target, line)
        target = 'set AllA_glycam [list'
        if(line.find(target) > 0):
            AllA_glycam = get_res_list(target, line)

        target = 'set TalA_common [list'
        if(line.find(target) > 0):
            TalA_common = get_res_list(target, line)
        target = 'set TalA_glycam [list'
        if(line.find(target) > 0):
            TalA_glycam = get_res_list(target, line)

        target = 'set IdoA_common [list'
        if(line.find(target) > 0):
            IdoA_common = get_res_list(target, line)
        target = 'set IdoA_glycam [list'
        if(line.find(target) > 0):
            IdoA_glycam = get_res_list(target, line)

        ### Filled cone ###
        target = 'set Qui_common [list'
        if(line.find(target) > 0):
            Qui_common = get_res_list(target, line)
        target = 'set Qui_glycam [list'
        if(line.find(target) > 0):
            Qui_glycam = get_res_list(target, line)

        target = 'set Rha_common [list'
        if(line.find(target) > 0):
            Rha_common = get_res_list(target, line)
        target = 'set Rha_glycam [list'
        if(line.find(target) > 0):
            Rha_glycam = get_res_list(target, line)

        target = 'set x6dAlt_common [list'
        if(line.find(target) > 0):
            x6dAlt_common = get_res_list(target, line)
        target = 'set x6dAlt_glycam [list'
        if(line.find(target) > 0):
            x6dAlt_glycam = get_res_list(target, line)

        target = 'set x6dTal_common [list'
        if(line.find(target) > 0):
            x6dTal_common = get_res_list(target, line)
        target = 'set x6dTal_glycam [list'
        if(line.find(target) > 0):
            x6dTal_glycam = get_res_list(target, line)

        target = 'set Fuc_common [list'
        if(line.find(target) > 0):
            Fuc_common = get_res_list(target, line)
        target = 'set Fuc_glycam [list'
        if(line.find(target) > 0):
            Fuc_glycam = get_res_list(target, line)

        ### Divided cone ###
        target = 'set QuiNAc_common [list'
        if(line.find(target) > 0):
            QuiNAc_common = get_res_list(target, line)
        target = 'set QuiNAc_glycam [list'
        if(line.find(target) > 0):
            QuiNAc_glycam = get_res_list(target, line)

        target = 'set RhaNAc_common [list'
        if(line.find(target) > 0):
            RhaNAc_common = get_res_list(target, line)
        target = 'set RhaNAc_glycam [list'
        if(line.find(target) > 0):
            RhaNAc_glycam = get_res_list(target, line)

        target = 'set FucNAc_common [list'
        if(line.find(target) > 0):
            FucNAc_common = get_res_list(target, line)
        target = 'set FucNAc_glycam [list'
        if(line.find(target) > 0):
            FucNAc_glycam = get_res_list(target, line)

        ### Flat rectangle ###
        target = 'set Oli_common [list'
        if(line.find(target) > 0):
            Oli_common = get_res_list(target, line)
        target = 'set Oli_glycam [list'
        if(line.find(target) > 0):
            Oli_glycam = get_res_list(target, line)

        target = 'set Tyv_common [list'
        if(line.find(target) > 0):
            Tyv_common = get_res_list(target, line)
        target = 'set Tyv_glycam [list'
        if(line.find(target) > 0):
            Tyv_glycam = get_res_list(target, line)

        target = 'set Abe_common [list'
        if(line.find(target) > 0):
            Abe_common = get_res_list(target, line)
        target = 'set Abe_glycam [list'
        if(line.find(target) > 0):
            Abe_glycam = get_res_list(target, line)

        target = 'set Par_common [list'
        if(line.find(target) > 0):
            Par_common = get_res_list(target, line)
        target = 'set Par_glycam [list'
        if(line.find(target) > 0):
            Par_glycam = get_res_list(target, line)

        target = 'set Dig_common [list'
        if(line.find(target) > 0):
            Dig_common = get_res_list(target, line)
        target = 'set Dig_glycam [list'
        if(line.find(target) > 0):
            Dig_glycam = get_res_list(target, line)

        target = 'set Col_common [list'
        if(line.find(target) > 0):
            Col_common = get_res_list(target, line)
        target = 'set Col_glycam [list'
        if(line.find(target) > 0):
            Col_glycam = get_res_list(target, line)

        ### Filled star ###
        target = 'set Ara_common [list'
        if(line.find(target) > 0):
            Ara_common = get_res_list(target, line)
        target = 'set Ara_glycam [list'
        if(line.find(target) > 0):
            Ara_glycam = get_res_list(target, line)

        target = 'set Lyx_common [list'
        if(line.find(target) > 0):
            Lyx_common  = get_res_list(target, line)
        target = 'set Lyx_glycam [list'
        if(line.find(target) > 0):
            Lyx_glycam = get_res_list(target, line)

        target = 'set Xyl_common [list'
        if(line.find(target) > 0):
            Xyl_common  = get_res_list(target, line)
        target = 'set Xyl_glycam [list'
        if(line.find(target) > 0):
            Xyl_glycam = get_res_list(target, line)

        target = 'set Rib_common [list'
        if(line.find(target) > 0):
            Rib_common  = get_res_list(target, line)
        target = 'set Rib_glycam [list'
        if(line.find(target) > 0):
            Rib_glycam = get_res_list(target, line)

        ### Filled diamond ###
        target = 'set Kdn_common [list'
        if(line.find(target) > 0):
            Kdn_common  = get_res_list(target, line)
        target = 'set Kdn_glycam [list'
        if(line.find(target) > 0):
            Kdn_glycam = get_res_list(target, line)

        target = 'set Neu5Ac_common [list'
        if(line.find(target) > 0):
            Neu5Ac_common  = get_res_list(target, line)
        target = 'set Neu5Ac_glycam [list'
        if(line.find(target) > 0):
            Neu5Ac_glycam = get_res_list(target, line)

        target = 'set Neu5Gc_common [list'
        if(line.find(target) > 0):
            Neu5Gc_common  = get_res_list(target, line)
            if len(Neu5Gc_common) == 0:
                Neu5Gc_common.append('NGC')
        target = 'set Neu5Gc_glycam [list'
        if(line.find(target) > 0):
            Neu5Gc_glycam = get_res_list(target, line)

        target = 'set Neu_common [list'
        if(line.find(target) > 0):
            Neu_common  = get_res_list(target, line)
        target = 'set Neu_glycam [list'
        if(line.find(target) > 0):
            Neu_glycam = get_res_list(target, line)

        ### Flat hexagon ###
        target = 'set Bac_common [list'
        if(line.find(target) > 0):
            Bac_common  = get_res_list(target, line)
        target = 'set Bac_glycam [list'
        if(line.find(target) > 0):
            Bac_glycam = get_res_list(target, line)

        target = 'set LDManHep_common [list'
        if(line.find(target) > 0):
            LDManHep_common  = get_res_list(target, line)
        target = 'set LDManHep_glycam [list'
        if(line.find(target) > 0):
            LDManHep_glycam = get_res_list(target, line)

        target = 'set Kdo_common [list'
        if(line.find(target) > 0):
            Kdo_common  = get_res_list(target, line)
        target = 'set Kdo_glycam [list'
        if(line.find(target) > 0):
            Kdo_glycam = get_res_list(target, line)

        target = 'set Dha_common [list'
        if(line.find(target) > 0):
            Dha_common  = get_res_list(target, line)
        target = 'set Dha_glycam [list'
        if(line.find(target) > 0):
            Dha_glycam = get_res_list(target, line)

        target = 'set DDManHep_common [list'
        if(line.find(target) > 0):
            DDManHep_common  = get_res_list(target, line)
        target = 'set DDManHep_glycam [list'
        if(line.find(target) > 0):
            DDManHep_glycam = get_res_list(target, line)

        target = 'set MurNAc_common [list'
        if(line.find(target) > 0):
            MurNAc_common  = get_res_list(target, line)
        target = 'set MurNAc_glycam [list'
        if(line.find(target) > 0):
            MurNAc_glycam = get_res_list(target, line)

        target = 'set MurNGc_common [list'
        if(line.find(target) > 0):
            MurNGc_common  = get_res_list(target, line)
        target = 'set MurNGc_glycam [list'
        if(line.find(target) > 0):
            MurNGc_glycam = get_res_list(target, line)

        target = 'set Mur_common [list'
        if(line.find(target) > 0):
            Mur_common  = get_res_list(target, line)
        target = 'set Mur_glycam [list'
        if(line.find(target) > 0):
            Mur_glycam = get_res_list(target, line)

        ### Flat pentagon ###
        target = 'set Api_common [list'
        if(line.find(target) > 0):
            Api_common  = get_res_list(target, line)
        target = 'set Api_glycam [list'
        if(line.find(target) > 0):
            Api_glycam = get_res_list(target, line)

        target = 'set Fruc_common [list'
        if(line.find(target) > 0):
            Fruc_common  = get_res_list(target, line)
        target = 'set Fruc_glycam [list'
        if(line.find(target) > 0):
            Fruc_glycam = get_res_list(target, line)

        target = 'set Tag_common [list'
        if(line.find(target) > 0):
            Tag_common  = get_res_list(target, line)
        target = 'set Tag_glycam [list'
        if(line.find(target) > 0):
            Tag_glycam = get_res_list(target, line)

        target = 'set Sor_common [list'
        if(line.find(target) > 0):
            Sor_common  = get_res_list(target, line)
        target = 'set Sor_glycam [list'
        if(line.find(target) > 0):
            Sor_glycam = get_res_list(target, line)

        target = 'set Psi_common [list'
        if(line.find(target) > 0):
            Psi_common  = get_res_list(target, line)
        target = 'set Psi_glycam [list'
        if(line.find(target) > 0):
            Psi_glycam = get_res_list(target, line)

    mds = glob.glob(input_dir+"/md.*")
    for mddir in mds:
        cwd = os.getcwd()
        os.chdir(mddir)
        #if os.path.isfile('md.cif'):
        if os.path.isfile('md.pdb'):
            print(mddir+'/md.pdb')
            with open('md_snfg.pdb', "w") as fw:
                with open('md.pdb', "r") as f:
                    cif = f.readlines()
                    for line in cif:
                        line_head = line[0:6]
                        if ((line_head.find('HETATM') >= 0) or (line_head.find('ATOM') >= 0)):
                            #res = line.strip().split()[5]
                            res = line.strip().split()[3]
                            ### Filled sphere ###
                            if ((res in Glc_glycam) and (len(Glc_common[0]) == 3)):
                                line = line.replace(res, Glc_common[0], 1)
                            if ((res in Man_glycam) and (len(Man_common[0]) == 3)):
                                line = line.replace(res, Man_common[0], 1)
                            if ((res in Gal_glycam) and (len(Gal_common[0]) == 3)):
                                line = line.replace(res, Gal_common[0], 1)
                            if ((res in Gul_glycam) and (len(Gul_common[1]) == 3)):
                                line = line.replace(res, Gul_common[1], 1)
                            if ((res in Alt_glycam) and (len(Alt_common[0]) == 3)):
                                line = line.replace(res, Alt_common[0], 1)
                            if ((res in All_glycam) and (len(All_common[0]) == 3)):
                                line = line.replace(res, All_common[0], 1)
                            if ((res in Tal_glycam) and (len(Tal_common[0]) == 3)):
                                line = line.replace(res, Tal_common[0], 1)
                            if ((res in Ido_glycam) and (len(Ido_common[0]) == 3)):
                                line = line.replace(res, Ido_common[0], 1)
                            ### Filled cube ###
                            if ((res in GlcNAc_glycam) and (len(GlcNAc_common[0]) == 3)):
                                line = line.replace(res, GlcNAc_common[0], 1)
                            if ((res in ManNAc_glycam) and (len(ManNAc_common[0]) == 3)):
                                line = line.replace(res, ManNAc_common[0], 1)
                            if ((res in GalNAc_glycam) and (len(GalNAc_common[0]) == 3)):
                                line = line.replace(res, GalNAc_common[0], 1)
                            if ((res in GulNAc_glycam) and (len(GulNAc_common[0]) == 3)):
                                line = line.replace(res, GulNAc_common[0], 1)
                            if ((res in AltNAc_glycam) and (len(AltNAc_common[0]) == 3)):
                                line = line.replace(res, AltNAc_common[0], 1)
                            if ((res in AllNAc_glycam) and (len(AllNAc_common[0]) == 3)):
                                line = line.replace(res, AllNAc_common[0], 1)
                            if ((res in TalNAc_glycam) and (len(TalNAc_common[0]) == 3)):
                                line = line.replace(res, TalNAc_common[0], 1)
                            if ((res in IdoNAc_glycam) and (len(IdoNAc_common[0]) == 3)):
                                line = line.replace(res, IdoNAc_common[0], 1)
                            ### Crossed cube ###
                            if ((res in GlcN_glycam) and (len(GlcN_common[0]) == 3)):
                                line = line.replace(res, GlcN_common[0], 1)
                            if ((res in ManN_glycam) and (len(ManN_common[0]) == 3)):
                                line = line.replace(res, ManN_common[0], 1)
                            if ((res in GalN_glycam) and (len(GalN_common[0]) == 3)):
                                line = line.replace(res, GalN_common[0], 1)
                            if ((res in GulN_glycam) and (len(GulN_common[0]) == 3)):
                                line = line.replace(res, GulN_common[0], 1)
                            if ((res in AltN_glycam) and (len(AltN_common[0]) == 3)):
                                line = line.replace(res, AltN_common[0], 1)
                            if ((res in AllN_glycam) and (len(AllN_common[0]) == 3)):
                                line = line.replace(res, AllN_common[0], 1)
                            if ((res in TalN_glycam) and (len(TalN_common[0]) == 3)):
                                line = line.replace(res, TalN_common[0], 1)
                            if ((res in IdoN_glycam) and (len(IdoN_common[0]) == 3)):
                                line = line.replace(res, IdoN_common[0], 1)
                            ### Divided diamond ###
                            if ((res in GlcA_glycam) and (len(GlcA_common[0]) == 3)):
                                line = line.replace(res, GlcA_common[0], 1)
                            if ((res in ManA_glycam) and (len(ManA_common[0]) == 3)):
                                line = line.replace(res, ManA_common[0], 1)
                            if ((res in GalA_glycam) and (len(GalA_common[0]) == 3)):
                                line = line.replace(res, GalA_common[0], 1)
                            if ((res in GulA_glycam) and (len(GulA_common[0]) == 3)):
                                line = line.replace(res, GulA_common[0], 1)
                            if ((res in AltA_glycam) and (len(AltA_common[0]) == 3)):
                                line = line.replace(res, AltA_common[0], 1)
                            if ((res in AllA_glycam) and (len(AllA_common[0]) == 3)):
                                line = line.replace(res, AllA_common[0], 1)
                            if ((res in TalA_glycam) and (len(TalA_common[0]) == 3)):
                                line = line.replace(res, TalA_common[0], 1)
                            if ((res in IdoA_glycam) and (len(IdoA_common[0]) == 3)):
                                line = line.replace(res, IdoA_common[0], 1)
                            ### Filled cone ###
                            if ((res in Qui_glycam) and (len(Qui_common[0]) == 3)):
                                line = line.replace(res, Qui_common[0], 1)
                            if ((res in Rha_glycam) and (len(Rha_common[0]) == 3)):
                                line = line.replace(res, Rha_common[0], 1)
                            if ((res in x6dAlt_glycam) and (len(x6dAlt_common[0]) == 3)):
                                line = line.replace(res, x6dAlt_common[0], 1)
                            if ((res in x6dTal_glycam) and (len(x6dTal_common[0]) == 3)):
                                line = line.replace(res, x6dTal_common[0], 1)
                            if ((res in Fuc_glycam) and (len(Fuc_common[0]) == 3)):
                                line = line.replace(res, Fuc_common[0], 1)
                            ### Divided cone ###
                            if ((res in QuiNAc_glycam) and (len(QuiNAc_common[0]) == 3)):
                                line = line.replace(res, QuiNAc_common[0], 1)
                            if ((res in RhaNAc_glycam) and (len(RhaNAc_common[0]) == 3)):
                                line = line.replace(res, RhaNAc_common[0], 1)
                            if ((res in FucNAc_glycam) and (len(FucNAc_common[0]) == 3)):
                                line = line.replace(res, FucNAc_common[0], 1)
                            ### Flat rectangle ###
                            if ((res in Oli_glycam) and (len(Oli_common[0]) == 3)):
                                line = line.replace(res, Oli_common[0], 1)
                            if ((res in Tyv_glycam) and (len(Tyv_common[0]) == 3)):
                                line = line.replace(res, Tyv_common[0], 1)
                            if ((res in Abe_glycam) and (len(Abe_common[0]) == 3)):
                                line = line.replace(res, Abe_common[0], 1)
                            if ((res in Par_glycam) and (len(Par_common[0]) == 3)):
                                line = line.replace(res, Par_common[0], 1)
                            if ((res in Dig_glycam) and (len(Dig_common[0]) == 3)):
                                line = line.replace(res, Dig_common[0], 1)
                            if ((res in Col_glycam) and (len(Col_common[0]) == 3)):
                                line = line.replace(res, Col_common[0], 1)
                            ### Filled star ###
                            if ((res in Ara_glycam) and (len(Ara_common[0]) == 3)):
                                line = line.replace(res, Ara_common[0], 1)
                            if ((res in Lyx_glycam) and (len(Lyx_common[0]) == 3)):
                                line = line.replace(res, Lyx_common[0], 1)
                            if ((res in Xyl_glycam) and (len(Xyl_common[1]) == 3)):
                                line = line.replace(res, Xyl_common[1], 1)
                            if ((res in Rib_glycam) and (len(Rib_common[0]) == 3)):
                                line = line.replace(res, Rib_common[0], 1)
                            ### Filled diamond ###
                            if ((res in Kdn_glycam) and (len(Kdn_common[0]) == 3)):
                                line = line.replace(res, Kdn_common[0], 1)
                            if ((res in Neu5Ac_glycam) and (len(Neu5Ac_common[0]) == 3)):
                                line = line.replace(res, Neu5Ac_common[0], 1)
                            if ((res in Neu5Gc_glycam) and (len(Neu5Gc_common[0]) == 3)):
                                line = line.replace(res, Neu5Gc_common[0], 1)
                            if ((res in Neu_glycam) and (len(Neu_common[0]) == 3)):
                                line = line.replace(res, Neu_common[0], 1)
                            ### Flat hexagon ###
                            if ((res in Bac_glycam) and (len(Bac_common[0]) == 3)):
                                line = line.replace(res, Bac_common[0], 1)
                            if ((res in LDManHep_glycam) and (len(LDManHep_common[0]) == 3)):
                                line = line.replace(res, LDManHep_common[0], 1)
                            if ((res in Kdo_glycam) and (len(Kdo_common[0]) == 3)):
                                line = line.replace(res, Kdo_common[0], 1)
                            if ((res in Dha_glycam) and (len(Dha_common[0]) == 3)):
                                line = line.replace(res, Dha_common[0], 1)
                            if ((res in DDManHep_glycam) and (len(DDManHep_common[0]) == 3)):
                                line = line.replace(res, DDManHep_common[0], 1)
                            if ((res in MurNAc_glycam) and (len(MurNAc_common[0]) == 3)):
                                line = line.replace(res, MurNAc_common[0], 1)
                            if ((res in MurNGc_glycam) and (len(MurNGc_common[0]) == 3)):
                                line = line.replace(res, MurNGc_common[0], 1)
                            if ((res in Mur_glycam) and (len(Mur_common[0]) == 3)):
                                line = line.replace(res, Mur_common[0], 1)
                            ### Flat pentagon ###
                            if ((res in Api_glycam) and (len(Api_common[0]) == 3)):
                                line = line.replace(res, Api_common[0], 1)
                            if ((res in Fruc_glycam) and (len(Fruc_common[0]) == 3)):
                                line = line.replace(res, Fruc_common[0], 1)
                            if ((res in Tag_glycam) and (len(Tag_common[0]) == 3)):
                                line = line.replace(res, Tag_common[0], 1)
                            if ((res in Sor_glycam) and (len(Sor_common[0]) == 3)):
                                line = line.replace(res, Sor_common[0], 1)
                            if ((res in Psi_glycam) and (len(Psi_common[0]) == 3)):
                                line = line.replace(res, Psi_common[0], 1)
                        fw.write(line)
        os.chdir(cwd)


def make_mmcifs(input_dir, maxit):
    mds = glob.glob(input_dir+"/md.*")
    for mddir in mds:
        cwd = os.getcwd()
        os.chdir(mddir)
        if os.path.isfile('md_snfg.pdb'):
            print(mddir+'/md_snfg.pdb')
            make_script_maxit(maxit)
            res = subprocess.run(
                    ['./maxit_snfg.sh'],
                    stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
        os.chdir(cwd)


def main():
    parser = create_parser()
    args = parser.parse_args()
    try:
        if args.config:
            config_file = args.config
        if not os.path.isfile(config_file):
            print("Error: not exist file : "+config_file)
            sys.exit()
        if args.input:
            input_dir = args.input
        if not os.path.isdir(input_dir):
            print("Error: not exist input directory : "+input_dir)
            sys.exit()
        (maxit) = read_config(config_file)
        convert_PDBs(input_dir)
        make_mmcifs(input_dir, maxit)
    finally:
        print("END")


if __name__ == "__main__":
    main()
