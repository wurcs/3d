#!/usr/bin/env python3
import argparse
import os
import sys
import re
import configparser
import subprocess


def create_parser():
    parser = argparse.ArgumentParser(
             description='USAGE: (python3) generate_rotamer.py -i path_IUPAC_list_file')
    parser.add_argument('-c', '--config', default='config', help='path of config file')
    parser.add_argument('-i', '--input', help='path of input IUPAC list file')
    parser.add_argument('-o', '--output', default='.', help='path of output directory containing rotamer PDBs')
    return parser


def read_config(config):
    cfg = configparser.ConfigParser()
    cfg.read(config)
    genrotamer = cfg["glycam_rotamer"]["genrotamer"].strip()
    return (genrotamer)


def generate_rotamers(input_file, output_dir, genrotamer):
    with open(input_file, "r") as f:
        n = 1
        lines = f.readlines()
        for line in lines:
            print(line.strip())
            outdir = output_dir+'/iupac.'+str(n)
            res = subprocess.run(
                    [genrotamer,'-s',line.strip(),'-o',outdir],
                    stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
            n+=1


def main():
    parser = create_parser()
    args = parser.parse_args()
    try:
        if args.config:
            config_file = args.config
        if not os.path.isfile(config_file):
            print("Error: not exist file : "+config_file)
            sys.exit()
        if args.input:
            input_file = args.input
        if not os.path.isfile(input_file):
            print("Error: not exist file : "+input_file)
            sys.exit()
        if args.output:
            output_dir = args.output
        if not os.path.isdir(output_dir):
            os.makedirs(output_dir)
        print('output directory : '+output_dir)
        (genrotamer) = read_config(config_file)
        generate_rotamers(input_file, output_dir, genrotamer)
    finally:
        print("END")


if __name__ == "__main__":
    main()
