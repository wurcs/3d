#!/usr/bin/env python3
import argparse
import os
import sys
import glob
import configparser
import subprocess


def create_parser():
    parser = argparse.ArgumentParser(
             description='USAGE: (python3) make_GLYCAM_PDBs_by_IUPAC.py -i path_IUPAC_list_file -o path_directory_PDBs')
    parser.add_argument('-c', '--config', default='config', help='path of config file')
    parser.add_argument('-i', '--input', help='path of input IUPAC list file')
    parser.add_argument('-o', '--output', default='.', help='path of output directory containing PDB files')
    return parser


def read_config(config):
    cfg = configparser.ConfigParser()
    cfg.read(config)
    leaprc_GLYCAM = cfg["GLYCAM_gmml"]["leaprc_GLYCAM"].strip()
    return (leaprc_GLYCAM)


def make_GLYCAM_PDBs_by_IUPAC(input_file, output_dir, prep):
    with open(input_file, "r") as f:
        n = 1
        lines = f.readlines()
        for line in lines:
            print(line.strip())
            pdb = output_dir+'/md.'+str(n)+'.pdb'
            res = subprocess.run(
                    ["build_GLYCAM_PDB_by_IUPAC",pdb,prep,line.strip()],
                    stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
            n+=1


def main():
    parser = create_parser()
    args = parser.parse_args()
    try:
        if args.config:
            config_file = args.config
        if not os.path.isfile(config_file):
            print("Error: not exist file : "+config_file)
            sys.exit()
        if args.input:
            input_file = args.input
        if not os.path.isfile(input_file):
            print("Error: not exist file : "+input_file)
            sys.exit()
        if args.output:
            output_dir = args.output
        if not os.path.isdir(output_dir):
            os.makedirs(output_dir)
        else:
            print("Error: already exists output directory : "+output_dir+ " please delete this.")
            sys.exit()
        (leaprc_GLYCAM) = read_config(config_file)
        preps = glob.glob(leaprc_GLYCAM+"/*.prep")
        for prep in preps:
            make_GLYCAM_PDBs_by_IUPAC(input_file, output_dir, prep)
            break
    finally:
        print("END")


if __name__ == "__main__":
    main()
