#!/usr/bin/env python3
import argparse
import os
import sys
import glob
import subprocess


def create_parser():
    parser = argparse.ArgumentParser(
             description='USAGE: (python3) make_PDB_by_IUPAC.py -i path_IUPAC_list_file -o path_directory_PDBs')
    parser.add_argument('-i', '--input', help='path of input directory containing PDB files')
    parser.add_argument('-o', '--output', default='.', help='path of output directory containing PDB files')
    return parser


def modify_PDBs(input_dir, output_dir):
    pdbs = glob.glob(input_dir+"/*.pdb")
    for pdb in pdbs:
        filename = os.path.basename(pdb).rsplit('.',1)[0]
        print(filename)
        outpdb = output_dir+'/'+filename+'.pdb'
        res = subprocess.run(
                ["add_TER_separate_residues_PDB.py","-i",pdb,"-o",outpdb],
                stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)


def main():
    parser = create_parser()
    args = parser.parse_args()
    try:
        if args.input:
            input_dir = args.input
        if not os.path.isdir(input_dir):
            print("Error: not exist input directory : "+input_dir)
            sys.exit()
        if args.output:
            output_dir = args.output
        if not os.path.isdir(output_dir):
            os.makedirs(output_dir)
        modify_PDBs(input_dir, output_dir)
    finally:
        print("END")


if __name__ == "__main__":
    main()
