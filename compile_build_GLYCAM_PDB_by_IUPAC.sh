#!/bin/bash

export GMMLHOME=/home/glyconavi/tools/gmml

g++ -std=c++0x -I $GMMLHOME -L$GMMLHOME/bin/ -Wl,-rpath,$GMMLHOME/bin/ build_GLYCAM_PDB_by_IUPAC.cc -lgmml -pthread -o build_GLYCAM_PDB_by_IUPAC
