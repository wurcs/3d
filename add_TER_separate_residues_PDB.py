#!/usr/bin/env python3
import argparse
import os
import sys


def create_parser():
    parser = argparse.ArgumentParser(
             description='USAGE: (python3) add_TER_separate_residues_PDB.py -i path_input_PDB -o path_output_PDB')
    parser.add_argument('-i', '--input', help='path of input PDB file')
    parser.add_argument('-o', '--output', help='path of output PDB file')
    return parser


def add_TER_separate_residues_PDB(input_file, output_file):
    with open(output_file, "w") as fw:
        with open(input_file, "r") as f:
            pdb = f.readlines()
        res_list = []
        res_pre = '1'
        for line in pdb:
            line_head = line[0:6]
            if line_head.find('LINK') != -1:
                res1 = line.strip().split()[4]
                res2 = line.strip().split()[8]
                res_list.append([int(res1),int(res2)])
            if line_head.find('ATOM') != -1:
                res = line.strip().split()[5]
                if res != res_pre:
                    if [int(res_pre),int(res)] in res_list:
                        pass
                    else:
                        fw.write('TER\n')
                res_pre = res
            fw.write(line)


def main():
    parser = create_parser()
    args = parser.parse_args()
    try:
        if args.input:
            input_file = args.input
        if not os.path.isfile(input_file):
            print("Error: not exist file : "+input_file)
            sys.exit()
        if args.output:
            output_file = args.output
        else:
            filename = os.path.basename(input_file).rsplit('.',1)[0]
            output_file = filename+'.addter.pdb'
            print('output file : '+output_file)
        add_TER_separate_residues_PDB(input_file, output_file)
    finally:
        print("END")


if __name__ == '__main__':
    main()
