#!/usr/bin/env python3
import cif
import json


def main():
    data = cif.__loadCIF__("md.cif")
    json.dump(data, open("md.json", "w"))

if __name__ == "__main__":
    main()
