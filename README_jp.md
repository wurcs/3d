% WURCS_3Dマニュアル
% 2023/3
% 野口研究所

# 概要

* 入力は多数のWURCSのlistファイルで、GlycanFormatConverter により、WURCS->IUPAC に変換
* 多数のIUPACを入力にして、GLYCAM gmml により、GLYCAM力場で多数の3次元PDBを作成。
* 多数のPDBで AMBERとGROMACSの MD算系を作成し、エネルギー極小化計算と平衡化MD計算を実行。計算系は真空中と水溶媒＋中和イオン系で選択可能。
* 平衡化MD計算で得られたPDBを、SDF、mmcif、mmjsonに変換。変換ツールはopenbabel, maxit, cif-parsersを使用。
* 平衡化DM計算で得られたPDBを、3D-SNFG表示可能なPDBに変換。maxitで3D-SNFG表示可能なmmcif作成。
* 多数のIUPACを入力にして、GLYCAM gmml により、ロータマー候補構造のPDBを作成。

# インストール

* GlycanFormatConverter
* GLYCAM gmml
* AmberTools
* GROMACS
* openbabel
* maxit
* cif-parsers

# 依存環境
* Python 3

# 実行前の準備

* configファイルの編集
* 入力WURCS listファイル (下記例ではO-glycan.tsv) の準備
* WURCS_3D/build_GLYCAM_PDB_by_IUPAC.cc のコンパイル
* collsugar/glycam_rotamer/genrotamer.cc のコンパイル

## configファイルの編集

計算をコントロールするための設定ファイル。  
'#' または ';' でコメントアウトできる。

| [GlycanFormatConverter] | 設定例                                  | 説明                                |
|:------------------------|:---------------------------------------|:-----------------------------------|
| jar                     | /home/user/GlycanFormatConverter-cli-master/target/GlycanFormatConverter-cli.jar | jarファイルのパス |
| export                  | GlycanWeb                               | オプション(GlycanWeb/IUPAC-Extended/IUPAC-Condensed etc.) |

| [GLYCAM_gmml] | 設定例                                                            | 説明                          |
|:--------------|:-----------------------------------------------------------------|:------------------------------|
| leaprc_GLYCAM | /home/user/gmml/dat/CurrentParams/leaprc_GLYCAM_06j-1_2014-03-14 | leaprc_GLYCAMディレクトリのパス |

| [AMBER] | 設定例                       | 説明                  |
|:--------|:----------------------------|:----------------------|
| amber   | /home/user/amber22/amber.sh | amber.shファイルのパス |

| [GROMACS] | 設定例                        | 説明               |
|:----------|:-----------------------------|:-------------------|
| gromacs   | /usr/local/gromacs/bin/GMXRC | GMXRCファイルのパス |

| [MAXIT] | 設定例                            | 説明                  |
|:--------|:---------------------------------|:----------------------|
| maxit   | /home/usr/maxit-v11.100-prod-src | maxitディレクトリのパス |

| [glycam_rotamer] | 設定例                                              | 説明                     |
|:-----------------|:----------------------------------------------------|:------------------------|
| genrotamer       | /home/glyconavi/collsugar/glycam_rotamer/genrotamer | genrotamerファイルのパス |

## 入力WURCS listファイル (下記例ではO-glycan.tsv) の準備

* ファイル内容例。1行中の 'WURCS=' 以下を入力配列とする。

`O-glycan core 1 WURCS=2.0/2,2,1/[a2112h-1a_1-5_2*NCC/3=O][a2112h-1b_1-5]/1-2/a3-b1`  
`O-glycan core 5 WURCS=2.0/1,2,1/[a2112h-1a_1-5_2*NCC/3=O]/1-1/a3-b1`  
`O-glycan core 7 WURCS=2.0/1,2,1/[a2112h-1a_1-5_2*NCC/3=O]/1-1/a6-b1`

## WURCS_3D/build_GLYCAM_PDB_by_IUPAC.cc のコンパイル

> cd WURCS_3D

* compile_build_GLYCAM_PDB_by_IUPAC.sh を編集して、インストールした GLYCAM gmml のパスを GMMLHOME= に記述。下記を実行して、build_GLYCAM_PDB_by_IUPAC 実行プログラムを作成。

> ./compile_build_GLYCAM_PDB_by_IUPAC.sh

## collsugar/glycam_rotamer/genrotamer.cc のコンパイル

> cd collsugar/glycam_rotamer

* Makefile を編集して、インストールした GLYCAM gmml のパスを GEMSHOME= に記述。make を実行して、genrotamer 実行プログラムを作成。

> make

# 実行コマンド

* 実行ディレクトリは任意。WURCSリストファイルを O-glycan.tsvとする。

> source (path of WURCS_3D)/WURCS_3DRC.sh

> convert_WURCS_to_IUPAC.py -i (path of WURCS list file)/O-glycan.tsv

> make_GLYCAM_PDBs_by_IUPAC.py -i O-glycan.GlycanWeb.iupac -o out_pdb

> modify_PDBs_for_AMBER.py -i out_pdb -o out_pdb_ter

> make_systems_AMBER_GROMACS.py -i out_pdb_ter -o out_md

> run_MDs.py -i out_md

> convert_PDB_files.py -i out_md

> convert_PDB_files_for_3D_SNFG.py -i out_md

> generate_rotamer.py -i O-glycan.GlycanWeb.iupac -o ./out_rot

# 各コマンド（プログラム）概要

| プログラム                       | 機能                            |
|:--------------------------------|:--------------------------------|
| WURCS_3DRC.sh                   | WURCS_3Dプログラムの実行環境設定。任意ディレクトリで実行可能にする。 |
| convert_WURCS_to_IUPAC.py       | WURCS listをIUPAC listにGlycanFormatConverterで変換             |
| make_GLYCAM_PDBs_by_IUPAC.py    | IUPAC listからGLYCAM力場のPDBをGLYCAM gmmlで作成                 |
| modify_PDBs_for_AMBER.py        | GLYCAM力場のPDBにTER挿入 (非結合の単糖間に)                       |
| make_systems_AMBER_GROMACS.py   | MD計算系の作成 (AMBERとGROMACS)(真空中または水溶媒＋中和イオン)    |
| run_MDs.py                      | 各系のエネルギー極小化計算と平衡化MD計算実行 (AMBERまたはGROMACS)  |
| convert_PDB_files.py            | 各系のPDBをsdf,mmcif,mmjsonに変換                               |
| convert_PDB_files_for_3D_snfg.py| 各系のPDBを3D-SNFG表示可能なPDB、mmcifに変換                     |
| generate_rotamer.py             | IUPAC listからロータマー候補構造をGLYCAM gmmlで作成              |

# 実行結果の確認(上記コマンド例の場合)

## 実行ディレクトリ
* O-glycan.GlycanWeb.iupac ファイル  
(O-glycan.tsvファイルのWURCSをIUPACに変換したリストファイル)

## 実行ディレクトリ/out_pdb
* md.1.pdb md.2.pdb ... ファイル  
(IUPACの個数だけGLYCAM力場の3次元化PDBを出力)

## 実行ディレクトリ/out_pdb_ter
* md.1.pdb md.2.pdb ... ファイル  
(結合してない単糖間にTERを追記したPDBを出力)

## 実行ディレクトリ/out_md
* md.1 md.2 ... ディレクトリ  
(各PDBについてAMBERとGROMACSのMD計算系をすべて出力、MD計算に必要なファイルをすべて含む)
(ここで各系のMD計算を実行し、PDB,sdf,mmcif,mmjsonファイルも出力)

## 実行ディレクトリ/out_rot
* iupac.1 iupac.2 ... ディレクトリ  
(各IUPACについてロータマー候補構造のPDBを出力、候補構造がない場合は空)



