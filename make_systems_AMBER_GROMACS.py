#!/usr/bin/env python3
import argparse
import os
import sys
import glob
import shutil
import configparser
import subprocess


def create_parser():
    parser = argparse.ArgumentParser(
             description='USAGE: (python3) make_systems_AMBER_GROMACS.py -i path_directory_PDBs -o path_directory_MD_systems (-nw)')
    parser.add_argument('-c', '--config', default='config', help='path of config file')
    parser.add_argument('-i', '--input', help='path of input directory containing PDB files')
    parser.add_argument('-o', '--output', default='.', help='path of output directory containing MD systems')
    parser.add_argument('-nw', '--nowater', action='store_true', help='make MD system without water and ions')
    return parser


def read_config(config):
    cfg = configparser.ConfigParser()
    cfg.read(config)
    leaprc_GLYCAM = cfg["GLYCAM_gmml"]["leaprc_GLYCAM"].strip()
    amber = cfg["AMBER"]["amber"].strip()
    gromacs = cfg["GROMACS"]["gromacs"].strip()
    wurcs_3d = os.environ.get('WURCS_3D_HOME')
    return (leaprc_GLYCAM, amber, gromacs, wurcs_3d)


def make_tleapin_file(output_md_dir, pdb, leaprc, water_ion):
    with open(output_md_dir+'/tleap.in', "w") as fw:
        line =  'source ' + leaprc + '\n'
        line += 'set default PBRadii mbondi3\n'
        line += 'mol = loadPDB '+os.path.basename(pdb)+'\n'
        if water_ion == 'yes':
            line += 'addIons2 mol Na+ 4\n'
            line += 'addIons2 mol Cl- 0\n'
            line += 'solvateBox mol TIP3PBOX 8.0\n'
        line += 'charge mol\n'
        pdbfilebasename = os.path.basename(pdb).rsplit('.',1)[0]
        line += 'saveAmberParm mol '+pdbfilebasename+'.parm '+pdbfilebasename+'.rst\n'
        line += 'savePDB mol '+pdbfilebasename+'.leap.pdb\n'
        line += 'quit\n'
        fw.write(line)


def make_script_tleap(output_md_dir, amber):
    with open(output_md_dir+'/tleap.sh', "w") as fw:
        line =  '#!/bin/bash\n'
        line += '\n'
        line += 'source ' + amber + '\n'
        line += 'tleap -f tleap.in >& tleap.log\n'
        fw.write(line)
    os.chmod(output_md_dir+'/tleap.sh',0o755)


def make_script_amber(output_md_dir, amber, pdb, water_ion):
    with open(output_md_dir+'/run_amber.sh', "w") as fw:
        line =  '#!/bin/bash\n'
        line += '\n'
        line += 'source ' + amber + '\n'
        line += '\n'
        pdbfilebasename = os.path.basename(pdb).rsplit('.',1)[0]
        line += 'sander -O -i min.in -o min.out -p '+pdbfilebasename+'.parm -c '+pdbfilebasename+'.rst -r min.rst -inf min.info\n'
        if water_ion == 'yes':
            line += 'sander -O -i md1.in -o md1.out -p '+pdbfilebasename+'.parm -c min.rst -r md1.rst -x md1.nc -inf md1.info\n'
            line += 'sander -O -i md2.in -o md2.out -p '+pdbfilebasename+'.parm -c md1.rst -r md2.rst -x md2.nc -inf md2.info\n'
            line += 'ambpdb -p '+pdbfilebasename+'.parm -c min.rst > min.pdb\n'
            line += 'ambpdb -p '+pdbfilebasename+'.parm -c md1.rst > md1.pdb\n'
            line += 'ambpdb -p '+pdbfilebasename+'.parm -c md2.rst > md2.pdb\n'
        elif water_ion == 'no':
            line += 'sander -O -i md.in -o md.out -p '+pdbfilebasename+'.parm -c min.rst -r md.rst -x md.nc -inf md.info\n'
            line += 'ambpdb -p '+pdbfilebasename+'.parm -c min.rst > min.pdb\n'
            line += 'ambpdb -p '+pdbfilebasename+'.parm -c md.rst > md.pdb\n'
        fw.write(line)
    os.chmod(output_md_dir+'/run_amber.sh',0o755)


def make_script_gromacs(output_md_dir, gromacs, pdb, water_ion):
    with open(output_md_dir+'/run_gromacs.sh', "w") as fw:
        line =  '#!/bin/bash\n'
        line += '\n'
        line += 'source ' + gromacs + '\n'
        line += '\n'
        pdbfilebasename = os.path.basename(pdb).rsplit('.',1)[0]
        if water_ion == 'yes':
            line += 'gmx grompp -p '+pdbfilebasename+'.top -f min.mdp -c '+pdbfilebasename+'.gro -o min.tpr -maxwarn 2\n'
            line += 'gmx mdrun -deffnm min\n'
            line += 'gmx editconf -f min.gro -o min.pdb\n'
            line += '\n'
            line += 'gmx grompp -p '+pdbfilebasename+'.top -f md1.mdp -c min.gro -o md1.tpr -maxwarn 2\n'
            line += 'gmx mdrun -deffnm md1\n'
            line += 'gmx editconf -f md1.gro -o md1.pdb\n'
            line += '\n'
            line += 'gmx grompp -p '+pdbfilebasename+'.top -f md2.mdp -c md1.gro -o md2.tpr -maxwarn 2\n'
            line += 'gmx mdrun -deffnm md2\n'
            line += 'gmx editconf -f md2.gro -o md2.pdb\n'
        elif water_ion == 'no':
            line += 'gmx editconf -f '+pdbfilebasename+'.gro -d 2 -bt cubic -o box.gro\n'
            line += 'gmx grompp -p '+pdbfilebasename+'.top -f min.mdp -c box.gro -o min.tpr -maxwarn 2\n'
            line += 'gmx mdrun -deffnm min\n'
            line += 'gmx editconf -f min.gro -o min.pdb\n'
            line += '\n'
            line += 'gmx grompp -p '+pdbfilebasename+'.top -f md.mdp -c min.gro -o md.tpr -maxwarn 2\n'
            line += 'gmx mdrun -deffnm md\n'
            line += 'gmx editconf -f md.gro -o md.pdb\n'
        fw.write(line)
    os.chmod(output_md_dir+'/run_gromacs.sh',0o755)


def make_MD_systems(input_dir, output_dir, leaprc_GLYCAM, amber, gromacs, wurcs_3d, water_ion):
    pdbs = glob.glob(input_dir+"/*.pdb")
    for pdb in pdbs:
        pdbfilebasename = os.path.basename(pdb).rsplit('.',1)[0]
        print(pdbfilebasename)
        output_md_dir = output_dir+'/'+pdbfilebasename
        if not os.path.isdir(output_md_dir):
            os.makedirs(output_md_dir)
        else:
            print("Error: already exists output directory : "+output_md_dir+ " please delete this.")
            sys.exit()
        shutil.copy(pdb, output_md_dir)
        ff_files = glob.glob(leaprc_GLYCAM+"/*")
        leaprc = ''
        for ff_file in ff_files:
            ff_filename = os.path.basename(ff_file)
            shutil.copy(ff_file, output_md_dir+'/'+ff_filename)
            #print(ff_filename)
            if ff_filename.startswith('leaprc.'):
                leaprc = ff_filename
        if leaprc == '':
            print("Error: not exist file : "+leaprc_GLYCAM+'/leaprc.*')
            sys.exit()
        make_tleapin_file(output_md_dir, pdb, leaprc, water_ion)
        make_script_tleap(output_md_dir, amber)
        make_script_amber(output_md_dir, amber, pdb, water_ion)
        make_script_gromacs(output_md_dir, gromacs, pdb, water_ion)
        cwd = os.getcwd()
        os.chdir(output_md_dir)
        res = subprocess.run(
                ["./tleap.sh"],
                stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
        shutil.copy(wurcs_3d+'/groconvert/groconvert.sh', '.')
        shutil.copy(wurcs_3d+'/groconvert/acpype.py', '.')
        shutil.copy(wurcs_3d+'/groconvert/gen_posre.pl', '.')
        res = subprocess.run(
                ["./groconvert.sh","-i",pdbfilebasename,"-o",pdbfilebasename,"-r"],
                stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
        ''' run MD '''
        shutil.copy(wurcs_3d+'/md/min.mdp', '.')
        if water_ion == 'yes':
            shutil.copy(wurcs_3d+'/md/min2.in', 'min.in')
            shutil.copy(wurcs_3d+'/md/md1.in', '.')
            shutil.copy(wurcs_3d+'/md/md2.in', '.')
            shutil.copy(wurcs_3d+'/md/md1.mdp', '.')
            shutil.copy(wurcs_3d+'/md/md2.mdp', '.')
        elif water_ion == 'no':
            shutil.copy(wurcs_3d+'/md/min1.in', 'min.in')
            shutil.copy(wurcs_3d+'/md/md.in', '.')
            shutil.copy(wurcs_3d+'/md/md.mdp', '.')
        os.chdir(cwd)


def main():
    parser = create_parser()
    args = parser.parse_args()
    try:
        if args.config:
            config_file = args.config
        if not os.path.isfile(config_file):
            print("Error: not exist file : "+config_file)
            sys.exit()
        if args.input:
            input_dir = args.input
        if not os.path.isdir(input_dir):
            print("Error: not exist input directory : "+input_dir)
            sys.exit()
        if args.output:
            output_dir = args.output
        if not os.path.isdir(output_dir):
            os.makedirs(output_dir)
        water_ion = 'yes'
        if args.nowater:
            water_ion = 'no'
        (leaprc_GLYCAM, amber, gromacs, wurcs_3d) = read_config(config_file)
        make_MD_systems(input_dir, output_dir, leaprc_GLYCAM, amber, gromacs, wurcs_3d, water_ion)
    finally:
        print("END")


if __name__ == "__main__":
    main()
