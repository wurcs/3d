#!/usr/bin/env python3
import argparse
import os
import sys
import glob
import shutil
import subprocess


def create_parser():
    parser = argparse.ArgumentParser(
             description='USAGE: (python3) run_MDs.py -i path_directory_MD_systems')
    parser.add_argument('-i', '--input', default='.', help='path of input directory containing MD systems')
    parser.add_argument('-md', '--md', help='MD program (gromacs/amber)')
    return parser


def make_PDB_glycam_only_gromacs(pdb, mddir):
    subdirname = os.path.basename(mddir)
    atomsymbols = []
    link_lines = ''
    conect_lines = ''
    if os.path.isfile(subdirname+'.pdb'):
        print(subdirname+'.pdb')
        with open(subdirname+'.pdb', "r") as f:
            lines = f.readlines()
            for line in lines:
                if 'WAT' in line:
                    pass
                elif 'Na+' in line:
                    pass
                elif 'Cl+' in line:
                    pass
                elif 'ATOM' in line:
                    atomsymbols.append('          '+line[76:78])
                elif line.startswith('LINK'):
                    link_lines+=line
                elif line.startswith('CONECT'):
                    conect_lines+=line
    else:
        pass
    with open('tmp.pdb', "w") as fw:
        fw.write(link_lines)
        with open(pdb, "r") as f:
            lines = f.readlines()
            n=0
            for line in lines:
                if 'WAT' in line:
                    pass
                elif 'Na+' in line:
                    pass
                elif 'Cl+' in line:
                    pass
                elif 'ATOM' in line:
                    fw.write(line.strip()+atomsymbols[n]+'\n')
                    n+=1
        fw.write('TER\n')
        fw.write(conect_lines)
        fw.write('END\n')
    shutil.copyfile('tmp.pdb','md.pdb')
    os.remove('tmp.pdb')


def make_PDB_glycam_only_gromacs2(pdb, mddir):
    subdirname = os.path.basename(mddir)
    atomsymbols = []
    link_lines = ''
    conect_lines = ''
    if os.path.isfile(subdirname+'.pdb'):
        print(subdirname+'.pdb')
        with open(subdirname+'.pdb', "r") as f:
            lines = f.readlines()
            for line in lines:
                if 'WAT' in line:
                    pass
                elif 'Na+' in line:
                    pass
                elif 'Cl+' in line:
                    pass
                elif 'ATOM' in line:
                    atomsymbols.append('          '+line[76:78])
                elif line.startswith('LINK'):
                    link_lines+=line
                elif line.startswith('CONECT'):
                    conect_lines+=line
    else:
        pass
    with open('md.pdb', "w") as fw:
        fw.write(link_lines)
        with open(pdb, "r") as f:
            lines = f.readlines()
            n=0
            for line in lines:
                if 'WAT' in line:
                    pass
                elif 'Na+' in line:
                    pass
                elif 'Cl-' in line:
                    pass
                elif 'ATOM' in line:
                    fw.write(line.strip()+atomsymbols[n]+'\n')
                    n+=1
        fw.write('TER\n')
        fw.write(conect_lines)
        fw.write('END\n')


def make_PDB_glycam_only_amber(pdb, mddir):
    subdirname = os.path.basename(mddir)
    link_lines = ''
    conect_lines = ''
    if os.path.isfile(subdirname+'.pdb'):
        print(subdirname+'.pdb')
        with open(subdirname+'.pdb', "r") as f:
            lines = f.readlines()
            for line in lines:
                if line.startswith('LINK'):
                    link_lines+=line
                elif line.startswith('CONECT'):
                    conect_lines+=line
    else:
        pass
    with open('tmp.pdb', "w") as fw:
        fw.write(link_lines)
        with open(pdb, "r") as f:
            lines = f.readlines()
            for line in lines:
                if 'WAT' in line:
                    pass
                elif 'Na+' in line:
                    pass
                elif 'Cl-' in line:
                    pass
                elif 'ATOM' in line:
                    fw.write(line)
        fw.write('TER\n')
        fw.write(conect_lines)
        fw.write('END\n')
    shutil.copyfile('tmp.pdb','md.pdb')
    os.remove('tmp.pdb')


def make_PDB_glycam_only_amber2(pdb, mddir):
    subdirname = os.path.basename(mddir)
    link_lines = ''
    conect_lines = ''
    if os.path.isfile(subdirname+'.pdb'):
        print(subdirname+'.pdb')
        with open(subdirname+'.pdb', "r") as f:
            lines = f.readlines()
            for line in lines:
                if line.startswith('LINK'):
                    link_lines+=line
                elif line.startswith('CONECT'):
                    conect_lines+=line
    else:
        pass
    with open('md.pdb', "w") as fw:
        fw.write(link_lines)
        with open(pdb, "r") as f:
            lines = f.readlines()
            for line in lines:
                if 'WAT' in line:
                    pass
                elif 'Na+' in line:
                    pass
                elif 'Cl-' in line:
                    pass
                elif 'ATOM' in line:
                    fw.write(line)
        fw.write('TER\n')
        fw.write(conect_lines)
        fw.write('END\n')


def run_MD(input_dir, md):
    mds = glob.glob(input_dir+"/md.*")
    for mddir in mds:
        cwd = os.getcwd()
        os.chdir(mddir)
        if md == 'gromacs':
            runfile = 'run_gromacs.sh'
            if not os.path.isfile(runfile):
                print("Error: not exist file : "+runfile)
                sys.exit()
            else:
                print(mddir+'/'+runfile)
                res = subprocess.run(
                        ['./'+runfile],
                        stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
                if os.path.isfile('md2.pdb'):
                    make_PDB_glycam_only_gromacs2('md2.pdb', mddir)
                elif os.path.isfile('md.pdb'):
                    make_PDB_glycam_only_gromacs('md.pdb', mddir)
        elif md == 'amber':
            runfile = 'run_amber.sh'
            if not os.path.isfile(runfile):
                print("Error: not exist file : "+runfile)
                sys.exit()
            else:
                print(mddir+'/'+runfile)
                res = subprocess.run(
                        ['./'+runfile],
                        stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
                if os.path.isfile('md2.pdb'):
                    make_PDB_glycam_only_amber2('md2.pdb', mddir)
                elif os.path.isfile('md.pdb'):
                    make_PDB_glycam_only_amber('md.pdb', mddir)
        os.chdir(cwd)


def main():
    parser = create_parser()
    args = parser.parse_args()
    try:
        if args.input:
            input_dir = args.input
        if not os.path.isdir(input_dir):
            print("Error: not exist input directory : "+input_dir)
            sys.exit()
        if args.md:
            md = args.md
        else:
            print("Error: option -md gromacs/amber")
            sys.exit()
        print('MD program='+md)
        if not (md == 'gromacs' or md == 'amber'):
            print("Error: option -md gromacs/amber")
            sys.exit()
        run_MD(input_dir, md)
    finally:
        print("END")


if __name__ == "__main__":
    main()
